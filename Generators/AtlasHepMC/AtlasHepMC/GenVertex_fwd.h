/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_GENVERTEXFWD_H
#define ATLASHEPMC_GENVERTEXFWD_H
namespace HepMC {
class GenVertex;
typedef HepMC::GenVertex* GenVertexPtr;
}
#endif
